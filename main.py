import os

import click
import mlflow
import mlflow.pyfunc
import pandas as pd
from catboost import CatBoostClassifier, Pool
# from mlflow.models.signature import infer_signature
from sklearn.metrics import precision_score, recall_score, roc_auc_score
from sklearn.model_selection import train_test_split

from config.config import settings
from src.utils import fill_categorical_columns, get_categorical_indexes


@click.command()
@click.option(
    "--mode",
    default="train",
    help="python application mode",
    type=click.Choice(["train", "scoring"]),
)
@click.option(
    "--dataset",
    help="Path to train or test dataset in CSV format",
    required=True,
    type=str
)
# @click.option('--config', help='path to task config')
def run_app(mode, dataset):
    '''
    Main function for runnung Click-per-Install Service
    '''
    os.environ["AWS_ACCESS_KEY_ID"] = settings.AWS_ACCESS_KEY_ID
    os.environ["AWS_SECRET_ACCESS_KEY"] = settings.AWS_SECRET_ACCESS_KEY
    os.environ["MLFLOW_S3_ENDPOINT_URL"] = settings.MLFLOW_S3_ENDPOINT_URL
    mlflow.set_tracking_uri(settings.MLFLOW_URI)
    mlflow.set_registry_uri(settings.MLFLOW_URI)
    mlflow.set_experiment(settings.MLFLOW_RUN_NAME)

    if mode == "train":
        mlflow.autolog()
        df = pd.read_csv(dataset)
        df.drop(
            ["Unnamed: 0", "subs_id", "msisdn", "report_date"],
            axis=1, inplace=True)
        df = df[~((df.flag != 0) & (df.flag != 1))]
        df = fill_categorical_columns(df, "Nan")
        X_train, X_valid = train_test_split(
            df, test_size=0.25, shuffle=True,
            stratify=df["flag"], random_state=1)
        y_train = X_train.flag
        y_valid = X_valid.flag
        X_train.drop(columns=["flag"], inplace=True)
        X_valid.drop(columns=["flag"], inplace=True)
        cat_indices = get_categorical_indexes(X_train)
        train_pool = Pool(X_train, y_train, cat_features=cat_indices)
        valid_pool = Pool(X_valid, y_valid, cat_features=cat_indices)
        model = CatBoostClassifier(silent=True)
        model.fit(train_pool)
        y_score = model.predict(valid_pool)
        score = roc_auc_score(y_true=y_valid, y_score=y_score)
        precision = precision_score(y_true=y_valid, y_pred=y_score)
        recall = recall_score(y_true=y_valid, y_pred=y_score)
        mlflow.log_metric('roc-auc', score),
        mlflow.log_metric('precision', precision)
        mlflow.log_metric('recall', recall)
        model.save_model("./models/cpi_v_0.1.cbm")
        # signature = infer_signature(X_train, y_score)
        # mlflow.log_artifact(signature)
        mlflow.catboost.log_model(model, artifact_path="adtech_cpi",
                                  registered_model_name="Adtech_CPI")
    else:
        model_name = "Adtech_CPI"
        model_version = 5
        model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{model_version}")
        df = pd.read_csv(dataset)
        df.drop(
            ["Unnamed: 0", "subs_id", "msisdn", "report_date"],
            axis=1, inplace=True)
        df = fill_categorical_columns(df, "Nan")
        cat_indices = get_categorical_indexes(df)
        test_pool = Pool(df, cat_features=cat_indices)
        # model = CatBoostClassifier(silent=True)
        # model.load_model("./models/cpi_v_0.1.cbm")
        # preds = model.predict(test_pool)
        preds = model.predict(test_pool)
        preds = pd.DataFrame(preds)
        preds.to_csv("./data/predicts.csv")


if __name__ == "__main__":
    run_app()
