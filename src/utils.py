import pandas as pd


def fill_categorical_columns(df: pd.DataFrame, val: str) -> pd.DataFrame:
    """
    Function that fill Nan values in categorical columns
    for User provide string value
    """
    categorical_cols = [
        item[0] for item in zip(df.columns, df.dtypes) if item[1] == "object"
    ]
    df[categorical_cols] = df[categorical_cols].fillna(val)
    return df


def get_categorical_indexes(df: pd.DataFrame) -> list:
    """
    Get categorical columns indices list
    Need for Catboost Pool object
    """
    cat_cols = [item[0] for item in zip(df.columns, df.dtypes) if item[1] == "object"]
    cat_indices = []
    i = 0
    for col in df.columns:
        if col in cat_cols:
            cat_indices.append(i)
        i += 1
    return cat_indices
