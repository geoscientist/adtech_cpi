from pathlib import Path

from dynaconf import Dynaconf

settings = Dynaconf(
    envvar_prefix=False,
    environments=True,
    settings_files=[Path(__file__).parent / "settings.toml",
                    Path(__file__).parent / ".secrets.toml"]
)
